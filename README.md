# Seguimiento de proyecto en Wordpress con GitKraken

#### Herramientas:

1. [Bitnami con Wordpress](https://bitnami.com/stack/wordpress)

Primero, necesitamos instalar una version nueva de Wordpress y un servidor virtual en nuestra computadora.

existen muchas maneras de hacer esto, pero una de las formas más facil de realizarlo es usando Bitnami Wordpress Stack el cual instalará una copia de wordpress asi como tambien el servidor virtual.

Crea una carpeta en blanco en tu carpeta de sitios web, redirecciona a Bitnami.exe a esta localizacion e instala el stack ahí. El stack correrá independiente de otro stack o proyecto de sitio web en el que estes trabajando actualmente.

![Foto 1](img\1.jpg)

![Foto 2](img\2.jpg)

![Foto 3](img\3.jpg)

![Foto 4](img\4.jpg)

El manejador del stack instalará todos los archivos en la carpeta, incluyendo Apache2, una base de datos en PHP, mysql y más.

Ahora tenemos nuestro wordpress instalado y nueva maquina virual en nuestra maquina local. Usa el "manager-windows.exe" para lanzar el sitio, iniciar o detener el servidor y más.

![Foto 5](img\5.jpg)

¡CUIDADO! debes saber que una vez que el stack se haya instalado en una localidad, no se debe mover la carpeta donde se instaló el stack ya que el servidor local deja de funcionar al hacerlo.

Añadiendo nuestro proyecto a GitKraken

inicializa un GIT repo

En GitKraken podemos crear nuevos repositorios clickeando en el icono de archivo en lado superior izquierdo.

![Foto 6](img\gitkrak-new-repo.png)

selecciona el tab Init, navega hasta la carpeta de nuestro proyecto. asegurate de bajar un par de niveles dentro de nuestra carpeta app>wordpress o de otra manera, crearemos un GIT repo con el servidor apache, MySql, y los archivos de la maquina virtual, cosa que no queremos, ¡solo queremos los archivos de wordpress!

![Foto 7](img\init-tab.png)

![Foto 8](img\6.jpg)

ahora solo nos queda inicializar apache y MySql.

con esto ya podemos hacer nuestro primer commit con GitKraken en un proyecto de Wordpress. :D

## Push en Bitbucket

el siguiente paso es copiar el sitio a un repositorio online como Github o Bitbucket. Para este ejemplo utilizaremos Bitbucket y continuaremos usando GitKraken.

Primero debes linkear tu GitKraken a tu cuenta de Bitbucket. Esto se resuelve en la seccion "settings" de GitKraken.

Ve a tu cuenta de Bitbucket y crea un nuevo repositorio sin los archivos readme o .ignore. Nombra al repo como tu creas que sea apropiado, probablemente el mismo nombre que el proyecto de tu repositorio local. Luego ve a GitKraken y selecciona "add a remote"

![Foto 8](img\addremote.jpg)

![Foto 8](img\bit1.jpg)

poner nombre al repo y copiar tanto en pull como en push la url que entrega Bitbucket.
